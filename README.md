# FreshEvents

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]

Fresh Events is meant to be used with Laravel and Laravel Nova, and makes creating and managing events a breeze.

## Installation

Via Composer

``` bash
$ composer require freshinteractive/fresh-events
```

``` bash
$ php artisan fresh-events:install
```

Include in NovaServiceProvider.php:

``` injectablephp
/**
 * Register the application's Nova resources.
 *
 * @return void
 */
protected function resources()
{
    Nova::resourcesIn(app_path('Nova'));

    Nova::resources([
        \Freshinteractive\FreshEvents\Nova\Event::class,
        \Freshinteractive\FreshEvents\Nova\Asset::class,
        \Freshinteractive\FreshEvents\Nova\Session::class,
        \Freshinteractive\FreshEvents\Nova\TimeSlot::class,
        \Freshinteractive\FreshEvents\Nova\Speaker::class,
        \Freshinteractive\FreshEvents\Nova\Registrant::class, // Comment out if not using default registrant class.
    ]);
}
```

## Config

You can use your own "registrant" type model and nova resource by following the example below.

In this example, we will create a model and nova resource called Lead , and use this model as our "registrant" model and nova resource.

> *** IMPORTANT: the replacement "registrant" model must include a string 'email' field ***

config/fresh-events.php

``` injectablephp
<?php

use App\Models\Lead;
use App\Nova\Lead as LeadResource;

return [
    'registrant_model' => Lead::class, // the fully qualified class name to replace the default 'registrant_model' with.

    'registrant_nova_resource' => LeadResource::class // the fully qualified class name to replace the default 'registrant_nova_resource' with.
];
```

app/Models/Lead.php

``` injectablephp
<?php

namespace App\Models;

use Freshinteractive\FreshEvents\Traits\RegistrantRelationships;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    use RegistrantRelationships; // make sure to include this
    
    ...
    
    /**
     * Maps what fields should be downloaded by the 'ExportRegistrants' action.
     * In the form of 'field_name' => 'Heading Name'
     *
     * @return string[]
     */
    public static function downloadFields(): array
    {
        return [
            'name' => 'Name',
            'email' => 'Email'
        ];
    }
}
```

app/Nova/Lead.php

``` injectablephp
<?php

namespace App\Nova;

...

class Lead extends Resource
{
    use RegistrantRelationshipsFields; // includes relationshipFields function.
    
    ...
    
    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Fresh Events'; // include this if you'd like it to fall under the "Fresh Events" group in the Nova sidebar
    
    ...
    
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ...
            
             new Panel('Relationships', $this->relationshipFields()) // call the relationship fields
        ];
    }
    
    ...
    
     /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DownloadExcel())
                ->withFilename('leads-' . time() . '.csv')
                ->withHeadings()
        ];
    }

```

## Usage

Use the included Nova Resources as you would any other Nova Resource. 

#### Endpoints:

You can use these endpoints to retrieve any event related information that you need to build out a custom frontend.

- /api/fresh-events/events (events index)
- /api/fresh-events/events/{event:url} (events show)

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email dev@freshiscool.com instead of using the issue tracker.

## Credits

- [Fresh Interactive][link-author]
- [Laravel][link-laravel]
- [Laravel Nova][link-nova]
- [Spatie][link-spatie]
- [Ebess][link-ebess]
- [Maatwebsite][link-maatwebsite]

## License

MIT. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/freshinteractive/fresh-events.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/freshinteractive/fresh-events.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/freshinteractive/fresh-events/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/freshinteractive/fresh-events
[link-downloads]: https://packagist.org/packages/freshinteractive/fresh-events
[link-author]: https://bitbucket.org/freshinteractive
[link-laravel]: https://laravel.com
[link-nova]: https://nova.laravel.com
[link-spatie]: https://spatie.be
[link-ebess]: https://github.com/ebess
[link-maatwebsite]: https://github.com/Maatwebsite
