<?php

use Freshinteractive\FreshEvents\Models\Registrant;
use Freshinteractive\FreshEvents\Nova\Registrant as RegistrantResource;

return [
    'traffic_cop' => false,

    'include_time_slot_in_navigation' => false,

    'registrant_model' => Registrant::class,

    'registrant_nova_resource' => RegistrantResource::class,
];
