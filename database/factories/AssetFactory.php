<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Freshinteractive\FreshEvents\Models\Asset;
use Freshinteractive\FreshEvents\Models\Event;

$factory->define(Asset::class, function (Faker $faker) {
    return [
        'title' => rtrim($faker->sentence(3), '.'),
        'description' => $faker->paragraph(),
        'type' => $faker->randomElement(['image', 'pdf', 'website', 'video', 'other']),
        'link' => $faker->url(),
        'event_id' => factory(Event::class)
    ];
});
