<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Carbon\Carbon;
use Freshinteractive\FreshEvents\Models\Event;
use Illuminate\Support\Str;

$factory->define(Event::class, function (Faker $faker) {
    $title = rtrim($faker->sentence(3), '.');
    $start = Carbon::createFromTimestamp($faker->dateTimeBetween('+2 days', '+1 week')->getTimeStamp());
    $end= Carbon::createFromFormat('Y-m-d H:i:s', $start)->addHours($faker->numberBetween( 1, 8 ));

    return [
        'url' => Str::slug($title),
        'title' => $title,
        'event_type' => $faker->randomElement(['in_person', 'virtual', 'hybrid']),
        'description' => $faker->paragraph(),
        'post_event_description' => $faker->paragraph(),
        'start' => $start,
        'end' => $end
    ];
});
