<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Freshinteractive\FreshEvents\Models\Registrant;

$factory->define(Registrant::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName(),
        'email' => $faker->email(),
        'title' => $faker->title(),
        'company' => $faker->company(),
        'bio' => $faker->paragraph(),
        'website' => $faker->url()
    ];
});
