<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Freshinteractive\FreshEvents\Models\Speaker;

$factory->define(Speaker::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName(),
        'title' => $faker->title(),
        'company' => $faker->company(),
        'bio' => $faker->paragraph(),
        'website' => $faker->url()
    ];
});
