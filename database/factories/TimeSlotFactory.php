<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Carbon\Carbon;
use Freshinteractive\FreshEvents\Models\Event;
use Freshinteractive\FreshEvents\Models\TimeSlot;

$factory->define(TimeSlot::class, function (Faker $faker) {
    $start = Carbon::createFromTimestamp($faker->dateTimeBetween('+2 days', '+1 week')->getTimeStamp());
    $end= Carbon::createFromFormat('Y-m-d H:i:s', $start)->addHours($faker->numberBetween( 1, 8 ));

    return [
        'title' => rtrim($faker->sentence(3), '.'),
        'start' => $start,
        'end' => $end,
        'event_id' => factory(Event::class)
    ];
});
