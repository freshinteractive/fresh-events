<?php

use Illuminate\Support\Facades\Route;
use Freshinteractive\FreshEvents\Http\Controllers\EventController;

Route::get('/events', [EventController::class, 'index'])
    ->name('fresh-events.events.index');

Route::get('/events/{event:url}', [EventController::class, 'show']);
