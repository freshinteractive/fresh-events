<?php


namespace Freshinteractive\FreshEvents\Console;

use Illuminate\Support\Facades\File;


class InstallFreshEvents extends \Illuminate\Console\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fresh-events:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Fresh Events.';

    /**
     * The name of the package's config file.
     *
     * @var string
     */
    private string $configFile = 'fresh-events.php';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // start
        $this->info('Installing Fresh Events...');

        // publish config file
        $this->info('Publishing config file...');

        if (!$this->configExists($this->configFile)) {
            $this->publishConfig();
            $this->info('Published config file...');
        } else if ($this->shouldOverwriteConfig()) {
            $this->info('Overwriting config file...');
            $this->publishConfig(true);
        } else {
            $this->info('Existing config not overwritten.');
        }

        // publish migrations
        $this->info('Publishing migrations...');

        $this->publishMigrations();

        if ($this->shouldPublishRegistrantMigration()) {
            $this->info('Publishing registrant migration...');
            $this->publishRegistrantMigration();
        }

        if ($this->shouldRunMigrations()) {
            $this->call('migrate');
            $this->info('Running migrations...');
        } else {
            $this->info('Migrations not run.');
        }

        // finished
        $this->info('Fresh Events Installed!');
    }

    /**
     * Check if config file exists.
     *
     * @param string $fileName
     * @return bool
     */
    private function configExists(string $fileName): bool
    {
        return File::exists(config_path($fileName));
    }

    /**
     * Ask user if config should be overwritten.
     *
     * @return bool
     */
    private function shouldOverwriteConfig(): bool
    {
        return $this->confirm('Config file already exists. Do you want to overwrite it?');
    }

    /**
     * Publish the config file.
     *
     * @param bool $forcePublish
     */
    private function publishConfig(bool $forcePublish = false): void
    {
        $params = [
            '--provider' => 'Freshinteractive\FreshEvents\FreshEventsServiceProvider',
            '--tag' => 'fresh-events.config'
        ];

        if ($forcePublish === true) {
            $params['--force'] = '';
        }

        $this->call('vendor:publish', $params);
    }

    /**
     * Ask user if migrations should be run.
     *
     * @return bool
     */
    private function shouldRunMigrations(): bool
    {
        return $this->confirm('Run migrations now?');
    }

    /**
     * Publish migrations.
     */
    private function publishMigrations(): void
    {
        $params = [
            '--provider' => 'Freshinteractive\FreshEvents\FreshEventsServiceProvider',
            '--tag' => 'fresh-events.migrations'
        ];

        $this->call('vendor:publish', $params);
    }

    /**
     * Ask user if registrants table migration should be published.
     *
     * @return bool
     */
    private function shouldPublishRegistrantMigration(): bool
    {
        return $this->confirm('Include the "fevnt_registrants" table?');
    }

    /**
     * Publish registrant migration.
     */
    private function publishRegistrantMigration(): void
    {
        $params = [
            '--provider' => 'Freshinteractive\FreshEvents\FreshEventsServiceProvider',
            '--tag' => 'fresh-events.registrant-migration'
        ];

        $this->call('vendor:publish', $params);
    }
}
