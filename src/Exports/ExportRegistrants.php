<?php


namespace Freshinteractive\FreshEvents\Exports;

use Freshinteractive\FreshEvents\Traits\GetsNestedValues;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class ExportRegistrants extends DownloadExcel
{
    use GetsNestedValues;

    /**
     * Name of the action.
     *
     * @var string
     */
    public $name = 'Download Registrants';

    /**
     * Indicates if this action is only available on the resource detail view.
     *
     * @var bool
     */
    public $onlyOnDetail = true;

    /**
     * Maps what fields should be downloaded.
     * In the form of 'field_name' => 'Heading Name'
     *
     * @return array
     */
    public function downloadFields(): array
    {
        return method_exists(config('fresh-events.registrant_model'), 'downloadFields')
            ? config('fresh-events.registrant_model')::downloadFields()
            : [];
    }

    /**
     * Map rows for the spreadsheet.
     *
     * @param $row
     * @return array
     */
    public function map($row): array
    {
        if (method_exists(config('fresh-events.registrant_model'), 'mapExportRegistrants')) {
            $registrantModel = config('fresh-events.registrant_model');
            return (new $registrantModel)->mapExportRegistrants($row, $this->downloadFields());
        } else {
            $fields = array_keys($this->downloadFields());

            $title = $row->title ?? '';

            return $row->allRegistrants
                ->map(function ($r) use ($fields, $title) {
                    $regArr = ['title' => $title];

                    foreach ($fields as $field) {
                        $regArr[$field] = $this->getNestedValue($field, $r);
                    }

                    return $regArr;
                })
                ->toArray();
        }
    }

    /**
     * Headings for the spreadsheet.
     *
     * @return array
     */
    public function headings(): array
    {
        return array_merge(['Registered For'], array_values($this->downloadFields()));
    }
}
