<?php

namespace Freshinteractive\FreshEvents\Facades;

use Illuminate\Support\Facades\Facade;

class FreshEvents extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'fresh-events';
    }
}
