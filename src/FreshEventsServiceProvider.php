<?php

namespace Freshinteractive\FreshEvents;

use Freshinteractive\FreshEvents\Console\InstallFreshEvents;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class FreshEventsServiceProvider extends ServiceProvider
{
    /**
     * Array of migrations to publish in the form of: ClassName => FileName
     *
     * @var string[]
     */
    private array $migrations = [
        'CreateFevntEventsTable' => 'create_fevnt_events_table.php',
        'CreateFevntSessionsTable' => 'create_fevnt_sessions_table.php',
        'CreateFevntTimeSlotsTable' => 'create_fevnt_time_slots_table.php',
        'CreateFevntAssetsTable' => 'create_fevnt_assets_table.php',
        'CreateFevntSpeakersTable' => 'create_fevnt_speakers_table.php',
        'CreateFevntRegisterableTable' => 'create_fevnt_registerable_table.php',
    ];

    /**
     * Route configuration.
     *
     * @var array
     */
    private array $routeConfig = [
        'prefix' => 'api/fresh-events',
        'middleware' => ['api']
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot(): void
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'freshinteractive');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'freshinteractive');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->app->make(Factory::class)->load(__DIR__.'/../database/factories');
        $this->registerRoutes();

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/fresh-events.php', 'fresh-events');

        // Register the service the package provides.
        $this->app->singleton('fresh-events', function ($app) {
            return new FreshEvents;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return ['fresh-events'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/fresh-events.php' => config_path('fresh-events.php'),
        ], 'fresh-events.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/freshinteractive'),
        ], 'fresh-events.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/freshinteractive'),
        ], 'fresh-events.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/freshinteractive'),
        ], 'fresh-events.views');*/

        // Registering package commands.
        $this->commands([
            InstallFreshEvents::class
        ]);

        // Publishing Migrations.
        foreach ($this->migrations as $className => $fileName) {
            $this->publishMigration($className, $fileName);
        }

        // Publish registrant migration.
        $this->publishMigration(
            'CreateFevntRegistrantsTable',
            'create_fevnt_registrants_table.php',
            'fresh-events.registrant-migration'
        );
    }

    /**
     * Registers routes.
     */
    private function registerRoutes(): void
    {
        Route::group($this->routeConfig, function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        });
    }

    /**
     * Publish an individual migration.
     *
     * @param string $className
     * @param string $fileName
     * @param string $tag
     */
    private function publishMigration(string $className, string $fileName, string $tag = 'fresh-events.migrations'): void
    {
        if (!class_exists($className)) {
            $this->publishes([
                __DIR__.'/../database/migrations/'.$fileName.'.stub' => $this->getMigrationFileName($fileName),
            ], $tag);
        }
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param string $fileName
     * @return string
     */
    private function getMigrationFileName(string $fileName): string
    {
        $pattern = database_path('migrations\/' . '*' . $fileName);

        $existingFile = glob($pattern);

        if ($existingFile) {
            return $existingFile[0];
        }

        // ensure that events table is created first
        $date = ($fileName === 'create_fevnt_events_table.php')
            ? date('Y_m_d_His')
            : date('Y_m_d_His', (time() + 1));

        return database_path('migrations/' . $date . '_' . $fileName);
    }
}
