<?php


namespace Freshinteractive\FreshEvents\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Freshinteractive\FreshEvents\Models\Event;

class EventController extends Controller
{
    /**
     * List all events, with all relationships, ordered by date.
     *
     * @return Collection
     */
    public function index(): Collection
    {
        return Event::query()
            ->with(Event::includedRelationships())
            ->orderBy('start')
            ->get();
    }

    public function show(Event $event): Event
    {
        return $event
            ->load(Event::includedRelationships());
    }
}
