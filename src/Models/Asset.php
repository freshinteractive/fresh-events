<?php


namespace Freshinteractive\FreshEvents\Models;

use GTerrusa\FrontendMedialibrary\HasFrontendMedia;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Asset extends \Illuminate\Database\Eloquent\Model implements HasMedia
{
    use InteractsWithMedia, HasFrontendMedia;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fevnt_assets';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'frontend_media'
    ];

    // relationships

    /**
     * Get this asset's event.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    // Spatie media library functions

    /**
     * Spatie media library media collections.
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('file')->singleFile();

        $this->addMediaCollection('thumbnail')->singleFile();
    }

    /**
     * Spatie media library media conversions.
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('optimized')
            ->fit(Manipulations::FIT_MAX, 1500, 1500);

        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_MAX, 250, 250);
    }

}
