<?php


namespace Freshinteractive\FreshEvents\Models;

use Freshinteractive\FreshEvents\Traits\HasRegistrants;
use Freshinteractive\FreshEvents\Traits\HasSpeakers;
use Freshinteractive\FreshEvents\Traits\HasState;
use GTerrusa\FrontendMedialibrary\HasFrontendMedia;
use Illuminate\Support\Carbon;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Tags\HasTags;
use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

class Event extends \Illuminate\Database\Eloquent\Model implements HasMedia
{
    use HasRegistrants, HasSpeakers, InteractsWithMedia, HasFrontendMedia, HasTags, HasState, HasRelationships;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fevnt_events';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'start' => 'datetime',
        'end' => 'datetime',
        'registration_close_date' => 'datetime',
        'confirmation_email_date' => 'datetime',
        'party_size_options' => 'array'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'computedEnd',
        'confirmation_email_date',
        'event_state',
        'frontend_media'
    ];

    // relationships

    /**
     * Get this event's sessions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Session::class);
    }

    /**
     * Get this event's time slots.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timeSlots(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TimeSlot::class);
    }

    /**
     * Get this event's assets.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assets(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Asset::class);
    }

    /**
     * Get this event's time slots' registrants.
     *
     * @return \Staudenmeir\EloquentHasManyDeep\HasManyDeep
     */
    public function timeSlotsRegistrants()
    {
        return $this
            ->hasManyDeep(
                config('fresh-events.registrant_model'),
                [TimeSlot::class, 'fevnt_registerable'],
                [null, ['registerable_type', 'registerable_id'], 'id'],
                [null, null, 'registrant_id']
            )
            ->withIntermediate(TimeSlot::class, [
                'title',
                'max_capacity',
                'max_party_size',
                'start',
                'end'
            ])
            ->withPivot('fevnt_registerable', ['status','party_size','details']);
    }


    // attributes

    /**
     * Set default end to the end of day of start of the event.
     *
     * @param $value
     * @return Carbon|null
     */
    public function getComputedEndAttribute()
    {
        if ($this->end) {
            return Carbon::create($this->end);
        }

        if ($this->start) {
            return $this->start->endOfDay();
        }

        return null;
    }


    /**
     * Set default confirmation email date to the day before the event's start date.
     *
     * @param $value
     * @return Carbon|null
     */
    public function getConfirmationEmailDateAttribute($value)
    {
        if ($value) {
            return Carbon::create($value);
        }

        if ($this->requires_registration && $this->start) {
            return $this->start->subDays(1);
        }

        return null;
    }

    /**
     * Get the event's state. ('pre', 'live', or 'post)
     *
     * @return string
     */
    public function getEventStateAttribute(): string
    {
        if ($this->start && $this->computedEnd) {
            return $this->getState($this->start, $this->computedEnd);
        }

        return 'pre';
    }

    /**
     * Get all the event's registrants.
     *
     * @return mixed
     */
    public function getAllRegistrantsAttribute()
    {
        return $this->registrants
            ->merge($this->timeSlotsRegistrants);
    }

    // Spatie media library functions

    /**
     * Spatie media library media collections.
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('hero')->singleFile();

        $this->addMediaCollection('register')->singleFile();
    }

    /**
     * Spatie media library media conversions.
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('optimized')
            ->fit(Manipulations::FIT_MAX, 1500, 1500);

        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_MAX, 250, 250);
    }

    // controller functions

    /**
     * The relationships to include by default with the event from a controller.
     *
     * @return string[]
     */
    public static function includedRelationships(): array
    {
        return [
            'sessions',
            'sessions.speakers',
            'sessions.tags',
            'timeSlots',
            'assets',
            'speakers',
            'speakers.tags',
            'tags',
        ];
    }
}
