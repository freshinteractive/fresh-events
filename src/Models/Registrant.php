<?php


namespace Freshinteractive\FreshEvents\Models;

use Freshinteractive\FreshEvents\Traits\RegistrantRelationships;
use GTerrusa\FrontendMedialibrary\HasFrontendMedia;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Registrant extends Authenticatable implements HasMedia
{
    use Notifiable, RegistrantRelationships, InteractsWithMedia, HasFrontendMedia;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fevnt_registrants';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'frontend_media'
    ];

    /**
     * Maps what fields should be downloaded by the 'ExportRegistrants' action.
     * In the form of 'field_name' => 'Heading Name'
     *
     * @return string[]
     */
    public static function downloadFields(): array
    {
        return [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'pivot.status' => 'Registrant Status'
        ];
    }

    /**
     * Options for the model's 'status' on the registerable table.
     *
     * @return string[]
     */
    public static function statusOptions(): array
    {
        return [
            'needs_action' => 'Needs Action',
            'confirmed' => 'Confirmed',
            'tentative' => 'Tentative',
            'cancelled' => 'Cancelled',
            'waitlisted' => 'Waitlisted'
        ];
    }


    // Spatie media library functions

    /**
     * Spatie media library media collections.
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')->singleFile();

        $this->addMediaCollection('hero')->singleFile();
    }

    /**
     * Spatie media library media conversions.
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('optimized')
            ->fit(Manipulations::FIT_MAX, 1500, 1500);

        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_MAX, 250, 250);
    }
}
