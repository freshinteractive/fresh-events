<?php


namespace Freshinteractive\FreshEvents\Models;

use Freshinteractive\FreshEvents\Traits\HasSpeakers;
use Freshinteractive\FreshEvents\Traits\HasState;
use GTerrusa\FrontendMedialibrary\HasFrontendMedia;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Tags\HasTags;

class Session extends \Illuminate\Database\Eloquent\Model implements HasMedia
{
    use InteractsWithMedia, HasTags, HasFrontendMedia, HasState, HasSpeakers;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fevnt_sessions';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'start' => 'datetime',
        'end' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'session_state',
        'frontend_media'
    ];

    // relationships

    /**
     * Get this session's event.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    // attributes

    /**
     * Get the session's state. ('pre', 'live', or 'post)
     *
     * @return string
     */
    public function getSessionStateAttribute()
    {
        if ($this->start && $this->end) {
            return $this->getState($this->start, $this->end);
        }

        return 'pre';
    }

    // Spatie media library functions

    /**
     * Spatie media library media collections.
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('hero')->singleFile();

        $this->addMediaCollection('thumbnail')->singleFile();

        $this->addMediaCollection('video_fallback')->singleFile();
    }

    /**
     * Spatie media library media conversions.
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('optimized')
            ->fit(Manipulations::FIT_MAX, 1500, 1500);

        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_MAX, 250, 250);
    }
}
