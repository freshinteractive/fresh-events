<?php


namespace Freshinteractive\FreshEvents\Models;

use GTerrusa\FrontendMedialibrary\HasFrontendMedia;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Tags\HasTags;

class Speaker extends \Illuminate\Database\Eloquent\Model implements HasMedia
{
    use InteractsWithMedia, HasFrontendMedia, HasTags;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fevnt_speakers';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'frontend_media'
    ];

    // relationships

    /**
     * Get this speaker's events.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function events(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphedByMany(
            Event::class,
            'speakerable',
            'fevnt_speakerable'
        );
    }

    /**
     * Get this speaker's sessions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function sessions(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphedByMany(
            Session::class,
            'speakerable',
            'fevnt_speakerable'
        );
    }

    // Spatie media library functions

    /**
     * Spatie media library media collections.
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')->singleFile();

        $this->addMediaCollection('hero')->singleFile();
    }

    /**
     * Spatie media library media conversions.
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('optimized')
            ->fit(Manipulations::FIT_MAX, 1500, 1500);

        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_MAX, 250, 250);
    }
}
