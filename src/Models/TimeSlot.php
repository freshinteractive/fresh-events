<?php


namespace Freshinteractive\FreshEvents\Models;

use Freshinteractive\FreshEvents\Traits\HasRegistrants;

class TimeSlot extends \Illuminate\Database\Eloquent\Model
{
    use HasRegistrants;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fevnt_time_slots';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'start' => 'datetime',
        'end' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'spots_remaining',
    ];

    // relationships

    /**
     * Get this time slot's event.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return int|mixed
     */
    public function getTotalRegistrantsAttribute()
    {
        return $this->registrants()->sum('party_size');
    }

    /**
     * @return mixed
     */
    public function getSpotsRemainingAttribute()
    {
        return $this->max_capacity - $this->totalRegistrants;
    }
}
