<?php


namespace Freshinteractive\FreshEvents\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;

class Asset extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \Freshinteractive\FreshEvents\Models\Asset::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title'
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Fresh Events';

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            new Panel('Basic Info', $this->basicFields()),

            new Panel('Images', $this->imagesFields()),

            new Panel('Relationships', $this->relationshipFields())
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * The basic fields for the resource.
     *
     * @return array
     */
    protected function basicFields(): array
    {
        return [
            Text::make('Title')->required(),

            Textarea::make('Description')
                ->hideFromIndex()
                ->nullable(),

            Select::make('Type')
                ->options([
                    'image' => 'Image',
                    'pdf' => 'PDF',
                    'website' => 'Website',
                    'video' => 'Video',
                    'powerpoint' => 'PowerPoint',
                    'other' => 'Other'
                ])
                ->displayUsingLabels()
                ->required(),

            Text::make('Link')
                ->hideFromIndex()
        ];
    }

    /**
     * The image fields for the resource.
     *
     * @return array
     */
    protected function imagesFields(): array
    {
        return [
            Files::make('File', 'file')
                ->conversionOnIndexView('thumb')
                ->customPropertiesFields([
                    Text::make('Alt'),
                    Text::make('Caption')
                ]),

            Images::make('Thumbnail', 'thumbnail')
                ->conversionOnIndexView('thumb')
                ->customPropertiesFields([
                    Text::make('Alt'),
                    Text::make('Caption')
                ]),
        ];
    }

    /**
     * The relationship fields for the resource.
     *
     * @return array
     */
    protected function relationshipFields(): array
    {
        return [
            BelongsTo::make('Event')
        ];
    }
}
