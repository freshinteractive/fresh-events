<?php


namespace Freshinteractive\FreshEvents\Nova;


use Froala\NovaFroalaField\Froala;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Whitecube\NovaFlexibleContent\Flexible;
use DigitalCreative\ConditionalContainer\ConditionalContainer;
use DigitalCreative\ConditionalContainer\HasConditionalContainer;
use Freshinteractive\FreshEvents\Exports\ExportRegistrants;
use Freshinteractive\FreshEvents\Nova\Fields\RegistrantStatusFields;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;
use Spatie\TagsField\Tags;

class Event extends Resource
{

    use HasConditionalContainer;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \Freshinteractive\FreshEvents\Models\Event::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'url',
        'title'
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Fresh Events';

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            new Panel('Basic Info', $this->basicFields()),

            new Panel('Date Info', $this->dateFields()),

            new Panel('Registration Info', $this->registrationFields()),

            new Panel('Address Info', $this->addressFields()),

            new Panel('Display Info', $this->imagesFields()),

            new Panel('Relationships', $this->relationshipFields())
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ExportRegistrants())
                ->withHeadings()
                ->withFilename('event-registrants' . time() . '.csv'),
        ];
    }

    /**
     * The basic fields for the resource.
     *
     * @return array
     */
    protected function basicFields(): array
    {
        return [
            Text::make('Title')->required(),

            Text::make('Url')->required(),

            Select::make('Event Type')
                ->options([
                    'in_person' => 'In Person',
                    'virtual' => 'Virtual',
                    'hybrid' => 'Hybrid'
                ])
                ->displayUsingLabels()
                ->required()
                ->hideFromIndex(),

            Tags::make('Tags')
                ->nullable()
                ->hideFromIndex(),

            Froala::make('Description')
                ->withFiles(env('MEDIA_DISK', 'public'))
                ->nullable()
                ->hideFromIndex(),

            Froala::make('Post Event Description')
                ->withFiles(env('MEDIA_DISK', 'public'))
                ->nullable()
                ->hideFromIndex(),
            //TODO: lets chat on this - removing since time slots rule all
            //Number::make('Max Capacity')
            //    ->nullable()
            //    ->hideFromIndex(),

            //Number::make('Max Party Size')
            //    ->nullable()
            //    ->hideFromIndex(),

            Boolean::make('Private Event', 'private'),
        ];
    }

    /**
     * The date fields for the resource.
     *
     * @return array
     */
    protected function dateFields(): array
    {
        return [
            DateTime::make('Start')
                ->required()
                ->hideFromIndex(),

            DateTime::make('End')
                ->nullable()
                ->hideFromIndex(),
        ];
    }

    /**
     * The registration fields for the resource.
     *
     * @return array
     */
    protected function registrationFields(): array
    {
        return [
            Boolean::make('Requires Registration')
                ->required()
                ->hideFromIndex(),

            DateTime::make('Registration Close Date')
                ->nullable()
                ->hideFromIndex(),

            DateTime::make('Confirmation Email Date')
                ->nullable()
                ->hideFromIndex(),
        ];
    }

    /**
     * The address fields for the resource.
     *
     * @return array
     */
    protected function addressFields(): array
    {
        return [
            Place::make('Address', 'address_line_1')
                ->nullable()
                ->hideFromIndex(),
            Text::make('Address Line 2')
                ->nullable()
                ->hideFromIndex(),
            Text::make('City')
                ->nullable()
                ->hideFromIndex(),
            Text::make('State')
                ->nullable()
                ->hideFromIndex(),
            Text::make('Postal Code')
                ->nullable()
                ->hideFromIndex(),
            Text::make('Latitude')
                ->nullable()
                ->hideFromIndex(),
            Text::make('Longitude')
                ->nullable()
                ->hideFromIndex(),
        ];
    }

    /**
     * The image fields for the resource.
     *
     * @return array
     */
    protected function imagesFields(): array
    {
        return [
            Images::make('Hero Image', 'hero')
                ->conversionOnIndexView('thumb')
                ->customPropertiesFields([
                    Text::make('Alt'),
                    Text::make('Caption')
                ]),

            Images::make('Register Image', 'register')
                ->conversionOnIndexView('thumb')
                ->customPropertiesFields([
                    Text::make('Alt'),
                    Text::make('Caption')
                ]),

            Select::make('Date Display')->options([
                'range' => 'Date Range',
                'list' => 'List of Days',
            ]),

            Boolean::make('Detailed Party Count', 'detailed_party_count'),

            ConditionalContainer::make([
                Flexible::make('Dropdown fields', 'party_size_options')
                    ->button('Add field')
                    ->addLayout('Label', 'label', [
                        Text::make('Label'),
                        Boolean::make('Exclude from party count', 'exclude')
                    ])
            ])
            ->if('detailed_party_count = 1'),

        ];
    }

    /**
     * The relationship fields for the resource.
     *
     * @return array
     */
    protected function relationshipFields(): array
    {
        return [
            HasMany::make('Sessions', 'sessions', Session::class),

            HasMany::make('Time Slots', 'timeSlots', TimeSlot::class),

            HasMany::make('Assets', 'assets', Asset::class),

            MorphToMany::make('Speakers', 'speakers', Speaker::class),

            MorphToMany::make('Registrants', 'registrants', config('fresh-events.registrant_nova_resource'))
                ->fields(new RegistrantStatusFields),
        ];
    }
}
