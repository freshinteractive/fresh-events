<?php


namespace Freshinteractive\FreshEvents\Nova\Fields;

use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

class RegistrantStatusFields
{
    /**
     * Get the pivot fields for the relationship.
     *
     * @return array
     */
    public function __invoke(): array
    {
        return [
            Select::make('Registrant Status', 'status')
                ->options( method_exists(config('fresh-events.registrant_model'), 'statusOptions')
                    ? config('fresh-events.registrant_model')::statusOptions()
                    : [
                        'needs_action' => 'Needs Action',
                        'confirmed' => 'Confirmed',
                        'tentative' => 'Tentative',
                        'cancelled' => 'Cancelled',
                        'waitlisted' => 'Waitlisted'
                    ]),

            Text::make('Registrant Party Size', 'party_size')
        ];
    }
}
