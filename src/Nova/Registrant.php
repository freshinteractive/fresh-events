<?php


namespace Freshinteractive\FreshEvents\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Freshinteractive\FreshEvents\Traits\RegistrantRelationshipsFields;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Registrant extends Resource
{
    use RegistrantRelationshipsFields;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \Freshinteractive\FreshEvents\Models\Registrant::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'email';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'first_name',
        'last_name',
        'email'
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Fresh Events';

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            new Panel('Basic Info', $this->basicFields()),

            new Panel('Social Info', $this->socialFields()),

            new Panel('Images', $this->imagesFields()),

            new Panel('Relationships', $this->relationshipFields())
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DownloadExcel())->withHeadings()
        ];
    }

    /**
     * The basic fields for the resource.
     *
     * @return array
     */
    protected function basicFields(): array
    {
        return [
            Text::make('First Name')->required(),

            Text::make('Last Name')->required(),

            Text::make('Email')->required(),

            Boolean::make('Marketing Opt In')
                ->hideFromIndex(),

            Text::make('Company')
                ->hideFromIndex()
                ->nullable(),

            Textarea::make('Bio')
                ->hideFromIndex()
                ->nullable(),
        ];
    }

    /**
     * The social fields for the resource.
     *
     * @return array
     */
    protected function socialFields(): array
    {
        return [
            Text::make('Website')
                ->hideFromIndex()
                ->nullable(),

            Text::make('Facebook')
                ->hideFromIndex()
                ->nullable(),

            Text::make('Twitter')
                ->hideFromIndex()
                ->nullable(),

            Text::make('Instagram')
                ->hideFromIndex()
                ->nullable(),

            Text::make('Linkedin')
                ->hideFromIndex()
                ->nullable(),

            Text::make('Github')
                ->hideFromIndex()
                ->nullable(),

            Text::make('Youtube')
                ->hideFromIndex()
                ->nullable(),

            Text::make('Meetup')
                ->hideFromIndex()
                ->nullable(),
        ];
    }

    /**
     * The image fields for the resource.
     *
     * @return array
     */
    protected function imagesFields(): array
    {
        return [
            Images::make('Avatar', 'avatar')
                ->conversionOnIndexView('thumb')
                ->customPropertiesFields([
                    Text::make('Alt'),
                    Text::make('Caption')
                ]),

            Images::make('Hero Image', 'hero')
                ->conversionOnIndexView('thumb')
                ->customPropertiesFields([
                    Text::make('Alt'),
                    Text::make('Caption')
                ]),
        ];
    }
}
