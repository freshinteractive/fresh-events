<?php


namespace Freshinteractive\FreshEvents\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;
use Spatie\TagsField\Tags;

class Session extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \Freshinteractive\FreshEvents\Models\Session::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title'
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Fresh Events';

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            new Panel('Basic Info', $this->basicFields()),

            new Panel('Date Info', $this->dateFields()),

            new Panel('Stream Info', $this->streamFields()),

            new Panel('Images', $this->imagesFields()),

            new Panel('Relationships', $this->relationshipFields())
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * The basic fields for the resource.
     *
     * @return array
     */
    protected function basicFields(): array
    {
        return [
            Text::make('Title')->required(),

            Textarea::make('Description')
                ->nullable()
                ->hideFromIndex(),

            Tags::make('Tags')
                ->nullable()
                ->hideFromIndex(),
        ];
    }

    /**
     * The date fields for the resource.
     *
     * @return array
     */
    protected function dateFields(): array
    {
        return [
            DateTime::make('Start')
                ->required()
                ->hideFromIndex(),

            DateTime::make('End')
                ->required()
                ->hideFromIndex(),
        ];
    }

    /**
     * The stream fields for the resource.
     *
     * @return array
     */
    protected function streamFields(): array
    {
        return [
            Select::make('Stream Type')
                ->options([
                    'iframe' => 'IFrame',
                    'external_link' => 'External Link',
                    'code_block' => 'Code Block'
                ])
                ->displayUsingLabels()
                ->required()
                ->hideFromIndex(),

            Textarea::make('Stream')
        ];
    }


    /**
     * The address fields for the resource.
     *
     * @return array
     */
    protected function imagesFields(): array
    {
        return [
            Images::make('Hero Image', 'hero')
                ->conversionOnIndexView('thumb')
                ->customPropertiesFields([
                    Text::make('Alt'),
                    Text::make('Caption')
                ]),

            Images::make('Thumbnail Image', 'thumbnail')
                ->conversionOnIndexView('thumb')
                ->customPropertiesFields([
                    Text::make('Alt'),
                    Text::make('Caption')
                ]),

            Images::make('Video Fallback Image', 'video_fallback')
                ->conversionOnIndexView('thumb')
                ->customPropertiesFields([
                    Text::make('Alt'),
                    Text::make('Caption')
                ]),
        ];
    }

    /**
     * The relationship fields for the resource.
     *
     * @return array
     */
    protected function relationshipFields(): array
    {
        return [
            BelongsTo::make('Event'),

            MorphToMany::make('Speakers', 'speakers', Speaker::class),
        ];
    }
}
