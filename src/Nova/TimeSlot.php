<?php


namespace Freshinteractive\FreshEvents\Nova;

use Freshinteractive\FreshEvents\Exports\ExportRegistrants;
use Freshinteractive\FreshEvents\Nova\Fields\RegistrantStatusFields;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;

class TimeSlot extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \Freshinteractive\FreshEvents\Models\TimeSlot::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title'
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Fresh Events';

    /**
     * Determine if this resource is available for navigation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    public static function availableForNavigation(Request $request)
    {
        return config('fresh-events.include_time_slot_in_navigation', false);
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            new Panel('Basic Info', $this->basicFields()),

            new Panel('Date Info', $this->dateFields()),

            new Panel('Relationships', $this->relationshipFields())
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * The basic fields for the resource.
     *
     * @return array
     */
    protected function basicFields(): array
    {
        return [
            Text::make('Title')->required(),

            Number::make('Max Capacity')
                ->nullable(),

            Number::make('Max Party Size')
                ->nullable()
                ->hideFromIndex(),

            Text::make('Total Registrants')->readonly(),

            Text::make('Slots Remaining', 'spotsRemaining')->readonly()
        ];
    }

    /**
     * The date fields for the resource.
     *
     * @return array
     */
    protected function dateFields(): array
    {
        return [
            DateTime::make('Start')
                ->required()
                ->hideFromIndex(),

            DateTime::make('End')
                ->required()
                ->hideFromIndex(),
        ];
    }

    /**
     * The relationship fields for the resource.
     *
     * @return array
     */
    protected function relationshipFields(): array
    {
        return [
            BelongsTo::make('Event'),

            MorphToMany::make('Registrants', 'registrants', config('fresh-events.registrant_nova_resource'))
                ->fields(new RegistrantStatusFields),
        ];
    }
}
