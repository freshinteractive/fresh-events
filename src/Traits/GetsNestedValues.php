<?php


namespace Freshinteractive\FreshEvents\Traits;


use Illuminate\Support\Collection;

trait GetsNestedValues
{
    /**
     * Gets the a nested value from a collection or array.
     *
     * @param string $key
     * @param array|Collection $collection
     * @return mixed
     */
    protected function getNestedValue(string $key, $collection)
    {
        $orKeys = explode('|', $key);

        foreach ($orKeys as $currentKey) {
            $keys = explode('.', $currentKey);

            $nestedVal = (empty($keys)) ? null : $collection;

            foreach ($keys as $k) {
                $nestedVal = $nestedVal[$k] ?? null;
            }

            if ($nestedVal) {
                break;
            }
        }

        return $nestedVal;
    }
}
