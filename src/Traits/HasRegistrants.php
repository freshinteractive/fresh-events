<?php


namespace Freshinteractive\FreshEvents\Traits;


trait HasRegistrants
{
    /**
     * Get this model's registrants.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function registrants(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(
            config('fresh-events.registrant_model'),
            'registerable',
            'fevnt_registerable',
            'registerable_id',
            'registrant_id'
        )->withPivot('status', 'party_size');
    }
}
