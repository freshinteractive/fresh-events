<?php


namespace Freshinteractive\FreshEvents\Traits;


use Freshinteractive\FreshEvents\Models\Speaker;

trait HasSpeakers
{
    /**
     * Get this model's registrants.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function speakers(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(
            Speaker::class,
            'speakerable',
            'fevnt_speakerable',
            'speakerable_id',
            'speaker_id'
        );
    }
}
