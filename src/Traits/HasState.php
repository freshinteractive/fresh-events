<?php


namespace Freshinteractive\FreshEvents\Traits;


use Illuminate\Support\Carbon;

trait HasState
{
    /**
     * Returns the state (pre, post, or live) based on the start and end times provided.
     *
     * @param Carbon $start
     * @param Carbon $end
     * @return string
     */
    public function getState(Carbon $start, Carbon $end): string
    {
        $now = Carbon::now();

        if ($now->betweenIncluded($start, $end)) {
            return 'live';
        }

        if ($now->isBefore($start)) {
            return 'pre';
        }

        return 'post';
    }
}
