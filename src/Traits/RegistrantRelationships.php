<?php


namespace Freshinteractive\FreshEvents\Traits;


use Freshinteractive\FreshEvents\Models\Event;
use Freshinteractive\FreshEvents\Models\TimeSlot;

trait RegistrantRelationships
{
    /**
     * Get this registrant's events.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function events(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphedByMany(
            Event::class,
            'registerable',
            'fevnt_registerable',
            'registrant_id'
        )->withPivot('status', 'party_size');
    }

    /**
     * Get this registrant's time slots.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function timeSlots(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphedByMany(
            TimeSlot::class,
            'registerable',
            'fevnt_registerable',
            'registrant_id'
        )->withPivot('status', 'party_size');
    }
}
