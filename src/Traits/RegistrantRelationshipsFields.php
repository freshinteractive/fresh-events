<?php


namespace Freshinteractive\FreshEvents\Traits;

use Freshinteractive\FreshEvents\Nova\Event;
use Freshinteractive\FreshEvents\Nova\Fields\RegistrantStatusFields;
use Freshinteractive\FreshEvents\Nova\TimeSlot;
use Laravel\Nova\Fields\MorphToMany;

trait RegistrantRelationshipsFields
{
    /**
     * The relationship fields for the resource.
     *
     * @return array
     */
    protected function relationshipFields(): array
    {
        return [
            MorphToMany::make('Events', 'events', Event::class)
                ->fields(new RegistrantStatusFields),

            MorphToMany::make('Time Slots', 'timeSlots', TimeSlot::class)
                ->fields(new RegistrantStatusFields),
        ];
    }
}
