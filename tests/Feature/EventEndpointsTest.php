<?php


namespace Freshinteractive\FreshEvents\Tests\Feature;

use Freshinteractive\FreshEvents\Models\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;

class EventEndpointsTest extends \Freshinteractive\FreshEvents\Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @throws \Throwable
     */
    function events_index_endpoint_returns_all_events()
    {
        factory(Event::class, 3)->create();

        $response = $this->get(route('fresh-events.events.index'));

        $response->assertStatus(200);

        $content = $response->decodeResponseJson();

        $this->assertCount(3, $content);
    }

    /**
     * @test
     */
    function events_show_endpoint_returns_event_data()
    {
        factory(Event::class)->create([
            'title' => 'Test',
            'url' => 'test'
        ]);

        $response = $this->json('GET', '/api/fresh-events/events/test');

        $response->assertJson(function (AssertableJson $json) {
            $json->where('url', 'test')
                ->where('title', 'Test')
                ->etc();
        });
    }
}
