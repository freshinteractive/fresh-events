<?php


namespace Freshinteractive\FreshEvents\Tests;

use Illuminate\Support\Facades\File;

class TestCase extends \Orchestra\Testbench\TestCase
{
    /**
     * General set up
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->withFactories(__DIR__.'/../database/factories');

        // additional setup
        $this->artisan('vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="migrations"');
        $this->artisan('vendor:publish --provider="Spatie\Tags\TagsServiceProvider" --tag="tags-migrations"');

        if (File::exists(config_path('fresh-events.php'))) {
            unlink(config_path('fresh-events.php'));
        }

        $this->artisan('fresh-events:install')
            ->expectsQuestion('Include the "fevnt_registrants" table?', true)
            ->expectsQuestion('Run migrations now?', true);
    }

    /**
     * Ignore package discovery from.
     *
     * @return array
     */
    public function ignorePackageDiscoveriesFrom()
    {
        return [];
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     */
    protected function getEnvironmentSetUp($app): void
    {
        // perform environment setup
    }

}
