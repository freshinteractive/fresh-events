<?php


namespace Freshinteractive\FreshEvents\Tests\Unit;

use Freshinteractive\FreshEvents\Models\Asset;
use Freshinteractive\FreshEvents\Models\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AssetTest extends \Freshinteractive\FreshEvents\Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    function an_asset_can_be_created()
    {
        $asset = factory(Asset::class)->create([
            'title' => 'Test Asset'
        ]);

        $this->assertEquals('Test Asset', $asset->title);
    }

    /**
     * @test
     */
    function an_asset_belongs_to_an_event()
    {
        $asset = factory(Asset::class)
            ->create([
                'event_id' => factory(Event::class)->create([
                    'title' => 'Test Asset\'s Event',
                    'url' => 'test-assets-event'
                ])
            ]);

        $this->assertEquals('test-assets-event', $asset->event->url);
    }
}
