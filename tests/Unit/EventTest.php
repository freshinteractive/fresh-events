<?php


namespace Freshinteractive\FreshEvents\Tests\Unit;

use Freshinteractive\FreshEvents\Models\Asset;
use Freshinteractive\FreshEvents\Models\Registrant;
use Freshinteractive\FreshEvents\Models\Session;
use Freshinteractive\FreshEvents\Models\Speaker;
use Freshinteractive\FreshEvents\Models\TimeSlot;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Freshinteractive\FreshEvents\Models\Event;

class EventTest extends \Freshinteractive\FreshEvents\Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    function an_event_can_be_created()
    {
        $event = factory(Event::class)->create([
            'url' => 'test-event',
            'title' => 'Test Event'
        ]);

        $this->assertEquals('Test Event', $event->title);
    }

    /**
     * @test
     */
    function an_event_has_sessions()
    {
        $event = factory(Event::class)->create();
        $sessions = factory(Session::class, 3)->create([
            'event_id' => $event->id
        ]);

        $this->assertCount(3, $event->sessions);
    }

    /**
     * @test
     */
    function an_event_has_time_slots()
    {
        $event = factory(Event::class)->create();
        $timeSlots = factory(TimeSlot::class, 3)->create([
            'event_id' => $event->id
        ]);

        $this->assertCount(3, $event->timeSlots);
    }

    /**
     * @test
     */
    function an_event_has_assets()
    {
        $event = factory(Event::class)->create();
        $assets = factory(Asset::class, 3)->create([
            'event_id' => $event->id
        ]);

        $this->assertCount(3, $event->assets);
    }

    /**
     * @test
     */
    function an_event_belongs_to_many_speakers()
    {
        $event = factory(Event::class)->create();
        $speakers = factory(Speaker::class, 3)->create();
        $event->speakers()->syncWithoutDetaching($speakers);

        $this->assertCount(3, $event->speakers);
    }

    /**
     * @test
     */
    function an_event_morphs_to_many_registrants()
    {
        $event = factory(Event::class)->create();
        $registrants = factory(Registrant::class, 3)->create();
        $event->registrants()->syncWithoutDetaching($registrants);

        $this->assertCount(3, $event->registrants);
    }
}
