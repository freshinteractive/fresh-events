<?php


namespace Freshinteractive\FreshEvents\Tests\Unit;

use Illuminate\Support\Facades\File;

class InstallFreshEventsTest extends \Freshinteractive\FreshEvents\Tests\TestCase
{
    private string $configFile = 'fresh-events.php';

    /**
     * @param false $runMigrations
     * @return \Illuminate\Testing\PendingCommand|int
     */
    private function runInstall(bool $runMigrations = false, bool $includeRegistrantsTable = true)
    {
        if (File::exists(config_path($this->configFile))) {
            unlink(config_path($this->configFile));
        }

        $this->assertFalse(File::exists(config_path($this->configFile)));

        return $this->artisan('fresh-events:install')
            ->expectsQuestion('Include the "fevnt_registrants" table?', $includeRegistrantsTable)
            ->expectsQuestion('Run migrations now?', $runMigrations);
    }

    /**
     * @test
     */
    function the_install_command_copies_the_configuration()
    {
        $this->runInstall();

        $this->assertTrue(File::exists(config_path($this->configFile)));

        unlink(config_path($this->configFile));
    }

    /**
     * @test
     */
    function when_a_config_file_exists_users_can_choose_to_overwrite_it()
    {
        $this->runInstall();

        $this->assertTrue(File::exists(config_path($this->configFile)));

        $this->artisan('fresh-events:install')
            ->expectsQuestion(
                'Config file already exists. Do you want to overwrite it?',
                'yes'
            )
            ->expectsOutput('Overwriting config file...')
            ->expectsQuestion('Include the "fevnt_registrants" table?', true)
            ->expectsQuestion('Run migrations now?', false);

        unlink(config_path($this->configFile));
    }

    /**
     * @test
     */
    function when_a_config_file_exists_users_can_choose_to_not_overwrite_it()
    {
        $this->runInstall();

        $this->assertTrue(File::exists(config_path($this->configFile)));

        $this->artisan('fresh-events:install')
            ->expectsQuestion(
                'Config file already exists. Do you want to overwrite it?',
                false
            )
            ->expectsOutput('Existing config not overwritten.')
            ->expectsQuestion('Include the "fevnt_registrants" table?', true)
            ->expectsQuestion('Run migrations now?', false);

        unlink(config_path($this->configFile));
    }

    /**
     * @test
     */
    function a_user_can_choose_to_run_migrations_now()
    {
        $this->runInstall(true)
            ->expectsOutput('Running migrations...');
    }

    /**
     * @test
     */
    function a_user_can_choose_not_to_run_migrations_not()
    {
        $this->runInstall()
            ->expectsOutput('Migrations not run.');
    }
}
