<?php


namespace Freshinteractive\FreshEvents\Tests\Unit;

use Freshinteractive\FreshEvents\Models\Event;
use Freshinteractive\FreshEvents\Models\TimeSlot;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegistrantTest extends \Freshinteractive\FreshEvents\Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    function a_registrant_can_be_created()
    {
        $registrant = factory(config('fresh-events.registrant_model'))->create([
            'email' => 'test@test.com'
        ]);

        $this->assertEquals('test@test.com', $registrant->email);
    }

    /**
     * @test
     */
    function a_registrant_is_morphed_by_many_events()
    {
        $registrant = factory(config('fresh-events.registrant_model'))->create();
        $events = factory(Event::class, 3)->create();
        $registrant->events()->syncWithoutDetaching($events);

        $this->assertCount(3, $registrant->events);
    }

    /**
     * @test
     */
    function a_registrant_is_morphed_by_many_time_slots()
    {
        $registrant = factory(config('fresh-events.registrant_model'))->create();
        $timeSlots = factory(TimeSlot::class, 3)->create();
        $registrant->timeSlots()->syncWithoutDetaching($timeSlots);

        $this->assertCount(3, $registrant->timeSlots);
    }
}
