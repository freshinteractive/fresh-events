<?php


namespace Freshinteractive\FreshEvents\Tests\Unit;

use Freshinteractive\FreshEvents\Models\Event;
use Freshinteractive\FreshEvents\Models\Session;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SessionTest extends \Freshinteractive\FreshEvents\Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    function a_session_can_be_created()
    {
        $session = factory(Session::class)->create([
            'title' => 'Test Session'
        ]);

        $this->assertEquals('Test Session', $session->title);
    }

    /**
     * @test
     */
    function a_session_belongs_to_an_event()
    {
        $session = factory(Session::class)
            ->create([
                'event_id' => factory(Event::class)->create([
                    'title' => 'Test Session\'s Event',
                    'url' => 'test-sessions-event'
                ])
            ]);

        $this->assertEquals('test-sessions-event', $session->event->url);
    }
}
