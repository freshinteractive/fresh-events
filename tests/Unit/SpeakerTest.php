<?php


namespace Freshinteractive\FreshEvents\Tests\Unit;

use Freshinteractive\FreshEvents\Models\Event;
use Freshinteractive\FreshEvents\Models\Speaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SpeakerTest extends \Freshinteractive\FreshEvents\Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    function a_speaker_can_be_created()
    {
        $speaker = factory(Speaker::class)->create([
            'first_name' => 'Test Speaker'
        ]);

        $this->assertEquals('Test Speaker', $speaker->first_name);
    }

    /**
     * @test
     */
    function a_speaker_belongs_to_many_events()
    {
        $speaker = factory(Speaker::class)->create();
        $events = factory(Event::class, 3)->create();
        $speaker->events()->syncWithoutDetaching($events);

        $this->assertCount(3, $speaker->events);
    }
}
