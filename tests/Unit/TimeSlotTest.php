<?php


namespace Freshinteractive\FreshEvents\Tests\Unit;

use Freshinteractive\FreshEvents\Models\Event;
use Freshinteractive\FreshEvents\Models\TimeSlot;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TimeSlotTest extends \Freshinteractive\FreshEvents\Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    function a_time_slot_can_be_created()
    {
        $timeSlot = factory(TimeSlot::class)->create([
            'title' => 'Test Time Slot'
        ]);

        $this->assertEquals('Test Time Slot', $timeSlot->title);
    }

    /**
     * @test
     */
    function a_time_slot_belongs_to_an_event()
    {
        $timeSlot = factory(TimeSlot::class)
            ->create([
                'event_id' => factory(Event::class)->create([
                    'title' => 'Test Time Slot\'s Event',
                    'url' => 'test-time-slots-event'
                ])
            ]);

        $this->assertEquals('test-time-slots-event', $timeSlot->event->url);
    }

    /**
     * @test
     */
    function a_time_slot_morphs_to_many_registrants()
    {
        $timeSlot = factory(TimeSlot::class)->create();
        $registrants = factory(config('fresh-events.registrant_model'), 3)->create();
        $timeSlot->registrants()->syncWithoutDetaching($registrants);

        $this->assertCount(3, $timeSlot->registrants);
    }
}
